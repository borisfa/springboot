package com.cyberark.items.web;


//import com.cyberark.exception.NotImplementedException;
import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemRuleType;
import com.cyberark.items.entities.ItemType;
import com.cyberark.items.services.ItemRuleService;
import com.cyberark.items.services.ItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class ItemController {
    private static Logger logger = Logger.getLogger(ItemController.class.getName());
    @Autowired private ItemService itemService;
    @Autowired private ItemRuleService itemRuleService;
 
    @GetMapping(path = "/api/getAllItems")
    public ResponseEntity<?>getAllItems() {
      try {
    	List<Item> items =itemService.getAllItems();
    	logger.info(" retrive " + items.size() + "items");
        ResponseEntity<List<Item>> entity= new ResponseEntity<>(items,HttpStatus.OK);
        return entity;
      } catch(Exception e){
    	  return new ResponseEntity<Exception>(e, HttpStatus.NOT_ACCEPTABLE);
      }
    }

    @GetMapping(path = "/api/items/{id}")
    public ResponseEntity<Item> getItem(@PathVariable(name = "id") Long itemId) {
          Item item = itemService.getItem(itemId);
          HttpStatus httpStatus=null;
          if(item!=null) {
        	httpStatus=HttpStatus.FOUND;  
          }
          else {
        	  httpStatus=HttpStatus.NOT_FOUND;
          }
        return  new ResponseEntity<>(item,httpStatus);
    }

    @PostMapping(path = "/api/addItem")
    @ResponseBody
    public ResponseEntity<Item> createItem(@RequestBody Item item) {
         itemService.addItem(item);
    	 ResponseEntity<Item> entity= new ResponseEntity<>(item,HttpStatus.CREATED);
    	 return entity;
    }
    
    @PutMapping(path = "/api/items/dailyUpdate")
    @ResponseBody
    public ResponseEntity<Void> dailyUpdate() {
          itemService.dailyUpdateItems();
         return new  ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/api/items/rules")
    @ResponseBody
    public ResponseEntity<Void> setItemRule(@RequestParam ItemType itemType, @RequestParam ItemRuleType itemRuleType) {
    	itemRuleService.setItemRule(itemRuleType);
    	itemRuleService.setItemType(itemType);
    	return new  ResponseEntity<>(HttpStatus.OK);
    }
}