package com.cyberark.items.services;

import com.cyberark.items.entities.ItemRuleType;
import com.cyberark.items.entities.ItemType;

public interface ItemRuleService {
	void setItemRule(ItemRuleType itemRuleType);
	void setItemType(ItemType itemType);
}
