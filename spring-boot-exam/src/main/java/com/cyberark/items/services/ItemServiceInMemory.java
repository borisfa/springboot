package com.cyberark.items.services;

import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemRuleType;
import com.cyberark.items.entities.ItemType;
import com.cyberark.items.factory.Factory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@PropertySources( @PropertySource("classpath:updateRules.properties"))
@Service
public class ItemServiceInMemory implements ItemService, ItemRuleService {
	private static Logger logger= Logger.getLogger(ItemServiceInMemory.class.getName());
	File configFile =null;
	private Map<Long, Item> items = new HashMap<>();

	ObjectMapper objectMapper = new ObjectMapper();
	public ItemServiceInMemory(){
		try {
			configFile=ResourceUtils.getFile("classpath:ItemsMap.json");
		} catch (FileNotFoundException e) {
			logger.log(Level.SEVERE, "Error" ,e);
		}
	}
	
	//ItemType itemType, @RequestParam ItemRuleType itemRuleType
	
	@Override
	public void setItemRule(ItemRuleType itemRuleType) {
		 for(Item item : items.values() ) {
		    	if( item instanceof ItemRule) {
		           ItemRule itemRule = (ItemRule) item;
		           itemRule.setRule();
		           logger.info(" rule has been update for  = "+ item.toString() + " on "+ LocalDate.now());
		    	}  
		   } 
	}
	
   
	@Override
    public void clearItems() {
       items.clear();
       logger.info(" items have been deleted ");;
    }

    @Override
    public List<Item> getAllItems() {
       return items.values().stream().collect(Collectors.toList());
    }

    @Override
    public Item getItem(long id) {
    	return this.items.get(id);
    }

    @Override
    public Item addItem(Item item) {
        items.put(item.getId(), item);
        logger.info("insert succeed "); 
        return item; 
    }

    @Override
    public void dailyUpdateItems() {
  
      for(Item item : items.values() ) {
    	if( item instanceof ItemRule) {
           ItemRule itemRule = (ItemRule) item;
           itemRule.updateByRule();
           logger.info(" rule has been update for  = "+ item.toString() + " on "+ LocalDate.now());
    	}  
      } 
    }
  
    @Scheduled(fixedRate = 60*5*1000)
   	public void performTask() throws IOException {
    	if(items!=null && items.size()>0) {
	    	objectMapper.writeValue(configFile, items);
			logger.info("insert succeed look " + configFile.getPath() + "file"); 
	    	logger.info(" items list  " + this.items); 
    	}
   	}
    public Map<Long, Item> getItems() {
		return items;
	}

	@Override
	public void setItemType(ItemType itemType) {
		 for(Item item : items.values() ) {
			 item.setType(itemType);
		 }
	}
	
	public static void main(String args[]) throws Exception {
		 Map <Long, Item >items = new HashMap<>();
    	 items.put(15L,Factory.createItem(15,ItemType.BANANA, 1, 8));
    	 items.put(21L,Factory.createItem(21, ItemType.T_SHIRT, 2, 120));
    	 items.put(11L,Factory.createItem(11, ItemType.T_SHIRT, 1, 12));
    	 
    	 ObjectMapper objectMapper = new ObjectMapper();
    	 System.out.println(objectMapper.writeValueAsString(items));
	}
}