package com.cyberark.items.factory;


import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemType;
import com.cyberark.realItems.model.FixedValueItem;
import com.cyberark.realItems.model.GainsValueWithAgeItem;
import com.cyberark.realItems.model.LosesValueWithAgeQuicklyItem;
import com.cyberark.realItems.model.RegularItem;


public class Factory {
	public static Item createItem(ItemType itemType, int daysToExpire, int price) {
		return createItem(-1, itemType, daysToExpire, price);
	}
	public static Item createItem(long id, ItemType itemType, int daysToExpire, int price) {
	   Item item=null;
		switch (itemType) {
			case T_SHIRT: case BASKETBALL:
				 item = new FixedValueItem(id, daysToExpire,price);
				break;
		     case SCOTCH_BOTTLE:
		    	 item = new GainsValueWithAgeItem(id,daysToExpire,price);
  	
		      case BANANA: 	 
		    	  item = new LosesValueWithAgeQuicklyItem(id,daysToExpire,price);
		  		break;
		  		
		  
			default:
				 item = new RegularItem(id,itemType, daysToExpire,price);		
				 break;
		}
	   return item;
   }
}

