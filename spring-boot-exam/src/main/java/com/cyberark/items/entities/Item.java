package com.cyberark.items.entities;

import java.util.logging.Logger;


public  class Item  {
	  private static Logger logger=Logger.getLogger(Item.class.getName());
	  protected long id;
	  protected ItemType type;
	  protected int daysToExpire;
	  protected Integer price;
	

    public Item() {}

    public Item(long id, ItemType type, int daysToExpire, int price) {
        this.id = id;
        this.setType(type);
        this.setDaysToExpire(daysToExpire);
        this.setPrice(price);
        logger.info(" init Item succeded ");
    }

    protected Item(ItemType type, int daysToExpire, int price) {
       this(-1, type,daysToExpire,price);
    }

    /* Generated getter and setter code */
    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public int getDaysToExpire() {
        return daysToExpire;
    }

    protected  void setDaysToExpire(int daysToExpire) {
    	this.daysToExpire=daysToExpire;
    }

    public int getPrice() {
        return price;
    }

    protected  void setPrice(Integer price) {
    	this.price=price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override 
    public boolean equals(Object another ) {
    	if(another instanceof Item) {
    		 Item item = (Item) another;
    		 return ( this.id==item.getId() && this.type == item.getType() &&
    				 this.price==item.getPrice() && this.daysToExpire==item.getDaysToExpire() ) ;
    	}
		return false;
    }
    
    @Override
     public int hashCode() {
        Long code = 47 * ( 31 * ( 19 * ( 13 *id +type.hashCode() ) + daysToExpire ) + price);
    	return code.intValue();
    }
  
	
    
	  @Override
	    public String toString() {
	        return "Item{" +
	                "id=" + id +
	                ", type='" + type + '\'' +
	                ", daysToExpire=" + daysToExpire +
	                ", price=" + price +
	                '}';
	    }
}
