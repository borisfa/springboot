package com.cyberark.realItems.model;



import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemType;

public class FixedValueItem extends Item {

	public FixedValueItem(long id, int daysToExpire, int price) {
		super(id, ItemType.T_SHIRT,  daysToExpire, price);
	}
}
