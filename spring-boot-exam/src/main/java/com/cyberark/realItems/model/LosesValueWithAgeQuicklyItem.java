package com.cyberark.realItems.model;

import org.springframework.scheduling.annotation.Scheduled;

import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemType;
import com.cyberark.items.services.ItemRule;
import com.cyberark.rules.Rule;

public  class LosesValueWithAgeQuicklyItem extends Item implements ItemRule {
	private Rule rule = new Rule();
	public LosesValueWithAgeQuicklyItem(long id, int daysToExpire, int price) {
		super(id,ItemType.BANANA,daysToExpire,price);
	}

	public void setRule() {
		rule.setPriceChange(1);
		rule.setDaysChange(-1);
	}
	@Scheduled(cron = "1 0 0 ? * 1/1 *")  // start 24:00:01 each  day starting Sunday 
	@Override
	public void updateByRule() {
		int daysChange = rule.getPriceChange();
		int priceChange = rule.getPriceChange();
		if(price < ( ItemRule.MAX_PRICE -priceChange)) {
			this.price+= priceChange;
			this.daysToExpire +=daysChange;	
		}
	}
}
