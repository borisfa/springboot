package com.cyberark.realItems.model;

import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemType;

public class RegularItem extends Item {

	public RegularItem(long id, ItemType itemType, int daysToExpire, int price) {
		super(id, itemType, daysToExpire, price);
	}
}
