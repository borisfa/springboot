package com.cyberark.realItems.model;


import org.springframework.scheduling.annotation.Scheduled;

import com.cyberark.items.entities.Item;
import com.cyberark.items.entities.ItemType;
import com.cyberark.items.services.ItemRule;
import com.cyberark.rules.Rule;

public class GainsValueWithAgeItem extends Item  implements ItemRule {

	private Rule rule = new Rule();
	public GainsValueWithAgeItem(long id, int daysToExpire, int price) {
		super(id,ItemType.SCOTCH_BOTTLE,daysToExpire,price);
	}


	@Override
	public void setRule() {
		rule.setPriceChange(1);
		
	}		
	@Override
	public void setPrice(Integer price) {
		  super.setPrice(price);
	}

	@Override
	@Scheduled(cron = "0 1 1 ? * *")
	public void updateByRule() {
		int daysChange = rule.getPriceChange();
		int priceChange = rule.getPriceChange();
		if(price < ( ItemRule.MAX_PRICE -priceChange)) {
			this.price+= priceChange;
			this.daysToExpire +=daysChange;	
		}
	}
}
