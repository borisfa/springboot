package com.cyberark.realItems.model;


	import org.springframework.scheduling.annotation.Scheduled;

	import com.cyberark.items.entities.Item;
	import com.cyberark.items.entities.ItemType;
	import com.cyberark.items.services.ItemRule;
    import com.cyberark.rules.Rule;
	

	public class LosesValueWithAgeItem extends Item implements ItemRule {
        Rule policyRule = new Rule(-3,-1);
		public LosesValueWithAgeItem(long id, ItemType itemType , int daysToExpire, int price) {
			super(id,itemType,daysToExpire,price);
		}

		@Override
		public void setRule() {
			policyRule.setPriceChange(-3);
			policyRule.setDaysChange(-1);	
		}	
		
		@Scheduled(cron = "1 0 0 ? * 1/1 *")  // start 24:00:01 each 3 days starting Sunday 
		@Override
		public void updateByRule() {
			int days = policyRule.getPriceChange();
			int price = policyRule.getPriceChange();
			if(daysToExpire> 0) {
				daysToExpire +=days;
				this.price+= price;
			}
		}

		
	}

