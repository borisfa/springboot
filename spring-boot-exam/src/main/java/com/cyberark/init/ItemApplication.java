package com.cyberark.init;

import javax.servlet.ServletContextListener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"com.cyberark.items.web","com.cyberark.items.services"})
public class ItemApplication extends SpringBootServletInitializer  {
	
	@Override
    protected SpringApplicationBuilder configure(
      SpringApplicationBuilder builder) {
        return builder.sources(ItemApplication.class);
    }

    public static void main(String[] args) throws JsonProcessingException {
       SpringApplication.run(ItemApplication.class, args);
    }
    
 /*   @Bean
    public ServletRegistrationBean<MyServlet> servletRegistrationBean() {
      ServletRegistrationBean<MyServlet> bean = new ServletRegistrationBean<MyServlet>(
          new MyServlet(), "/myServlet");
      return bean;
    }
   */ 
    @Bean
    public ServletListenerRegistrationBean<ServletContextListener> listenerRegistrationBean() {
      ServletListenerRegistrationBean<ServletContextListener> bean = 
          new ServletListenerRegistrationBean<>();
      bean.setListener(new MyServletContextListener());
      return bean;
    }
}


