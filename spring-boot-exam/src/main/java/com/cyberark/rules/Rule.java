package com.cyberark.rules;

public class Rule {
     private Integer priceChange;
     private Integer daysChange;
     
     public Rule() {
    	 priceChange=0;
    	 daysChange=0;
     }
     
     public Rule(int priceChange, int daysChange ) {
    	 this.priceChange=priceChange;
    	 this.daysChange=daysChange;
     }
     public int getPriceChange() {
		return priceChange;
	}
	public void setPriceChange(Integer priceChange) {
	    this.priceChange=priceChange;
	}
	public int getDaysChange() {
		return daysChange;
	}
	public void setDaysChange(Integer daysChange) {
		this.daysChange= daysChange;
	}	
}
